#include<stdio.h>
#define MAXsize 100
void addition(int r,int c,int m1[MAXsize][MAXsize],int m2[MAXsize][MAXsize]);
void multiplication(int r,int c,int m1[MAXsize][MAXsize],int m2[MAXsize][MAXsize]);
int x,y,z;
int main()
{
    int c,r,m1[MAXsize][MAXsize],m2[MAXsize][MAXsize];
    printf("Enter the number of columns:");
    scanf("%d",&c);
    printf("Enter the number of rows:");
    scanf("%d",&r);
    printf("Enter first matrix: \n");
    for(x=0;x<r;x++)
    {
        for(y=0;y<c;y++)
        {
          scanf("%d",&m1[x][y]);
        }
    }
    printf("Enter second matrix: \n");
    for(x=0;x<r;x++)
    {
        for(y=0;y<c;y++)
        {
          scanf("%d",&m2[x][y]);
        }
    }
    printf("Answer of addition\n");
    addition(r,c,m1,m2);
    printf("\n");
    printf("Answer of multiplication\n");
    multiplication(r,c,m1,m2);
}
void addition(int r,int c,int m1[MAXsize][MAXsize],int m2[MAXsize][MAXsize])
{
    for(x=0;x<r;x++)
    {
        for(y=0;y<c;y++)
        {
          printf("%d\t",(m1[x][y]+m2[x][y]));
        }
    printf("\n");
    }
}

void multiplication(int r,int c,int m1[MAXsize][MAXsize],int m2[MAXsize][MAXsize])
{
     int multiple[MAXsize][MAXsize];
     for(x=0;x<r;x++)
    {
        for(y=0;y<c;y++)
        {
          multiple[x][y]=0;
          for(z=0;z<c;z++)
          {
             multiple[x][y]+=m1[x][z]*m2[z][y];
          }
        }
    }
    for(x=0;x<r;x++)
    {
        for(y=0;y<c;y++)
        {
          printf("%d\t",(multiple[x][y]));
        }
    printf("\n");
    }
}
